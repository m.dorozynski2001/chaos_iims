import numpy as np

# Ustawienia układu
star_positions = np.array([[1, 2.1547005383792515], [-3.1547005383792515, -1.5773502691896258], [2.1547005383792515, -2.5773502691896258]])  # Początkowe pozycje gwiazd
star_velocities = np.array([[-1., -1.], [-1., -1.0001], [-2., -2.]])  # Początkowe prędkości gwiazd
planet_position = np.array([0., 0.])  # Początkowa pozycja planety
planet_velocity = np.array([-2.8, -3.])  # Początkowa prędkość planety

# Masy gwiazd i planety
m_star1 = 10.  # Masa pierwszej gwiazdy
m_star2 = 10.  # Masa drugiej gwiazdy
m_star3 = 10.  # Masa trzeciej gwiazdy
m_planet = 0.001  # Masa planety

G = 1.00001  # Stała grawitacyjna (wartość arbitralna dla uproszczenia)