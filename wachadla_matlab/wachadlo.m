clear all;

g = 9.81; % Acceleration due to gravity (m/s^2)
L = 1.0; % Length of the pendulum (m)
theta0 = 0.5*pi; % Initial angle (radians)
tspan = [0 10]; % Time span for simulation (seconds)
initial_conditions0 = [theta0 0]; % Initial conditions [theta0, omega0]
initial_conditions1 = [theta0+0.1 0]; % Initial conditions [theta0, omega0]

% Define the differential equation for the simple pendulum
pendulum_eqns = @(t, y) [y(2); -g*sin(y(1))/L];

opcje = odeset('RelTol', 1e-6, 'AbsTol', 1e-6);
[t0, y] = ode45(pendulum_eqns, tspan, initial_conditions0,opcje);
[t1, v] = ode45(pendulum_eqns, tspan, initial_conditions1,opcje);

% Extract theta (angle) from the solution
theta0 = y(:, 1);
theta1 = v(:, 1);

% Convert angle from radians to degrees
theta_deg0 = rad2deg(theta0);
theta_deg1 = rad2deg(theta1);


x0 = L * sin(theta0);
y0 = -L * cos(theta0);

x1 = L * sin(theta1);
y1 = -L * cos(theta1);

plot(x0, y0, 'LineWidth', 1.5);
hold on;
plot(x1, y1, 'LineWidth', 1.5);
xlabel('X Position (m)');
ylabel('Y Position (m)');
title('Trajectory of Double Pendulum');
legend('w1','w2');
grid on;


% Plot the amplitude over time graph
% plot(t, theta_deg, 'LineWidth', 1.5);
% xlabel('Time (s)');
% ylabel('Amplitude (degrees)');
% title('Amplitude of Simple Pendulum Over Time');
% grid on;
