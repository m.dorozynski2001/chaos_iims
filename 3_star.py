import matplotlib.pyplot as plt
import numpy as np
from matplotlib.animation import FuncAnimation

# Ustawienia układu
star_positions = [
    (1, 2.1547005383792515),
    (-3.1547005383792515, -1.5773502691896258),
    (2.1547005383792515, -2.5773502691896258),
]  # Pozycje gwiazd
planet_position = np.array([0.0, 0.0])  # Początkowa pozycja planety
planet_velocity = np.array([-2, -2.0])  # Początkowa prędkość planety

G = 1.0  # Stała grawitacyjna (wartość arbitralna dla uproszczenia)
m_star = 10.0  # Masa gwiazd (załóżmy, że wszystkie gwiazdy mają tę samą masę)
m_planet = 0.01  # Masa planety (dużo mniejsza od masy gwiazd)


# Funkcja do obliczania siły grawitacyjnej
def gravitational_force(pos1, pos2, m1, m2, G):
    r = np.linalg.norm(pos2 - pos1)
    force_magnitude = G * m1 * m2 / r**2
    force_direction = (pos2 - pos1) / r
    return force_magnitude * force_direction


# Funkcja do aktualizacji pozycji i prędkości planety
def update_position_velocity(
    planet_pos, planet_vel, star_positions, m_star, m_planet, G, dt
):
    total_force = np.array([0.0, 0.0])
    for star_pos in star_positions:
        force = gravitational_force(planet_pos, star_pos, m_planet, m_star, G)
        total_force += force

    # Aktualizacja prędkości
    planet_acceleration = total_force / m_planet
    planet_vel += planet_acceleration * dt

    # Aktualizacja pozycji
    planet_pos += planet_vel * dt

    return planet_pos, planet_vel


# Funkcja inicjalizacyjna do animacji
def init():
    line.set_data([], [])
    return (line,)


# Funkcja aktualizująca do animacji
def animate(i):
    global planet_position, planet_velocity
    planet_position, planet_velocity = update_position_velocity(
        planet_position, planet_velocity, star_positions, m_star, m_planet, G, dt
    )
    xdata.append(planet_position[0])
    ydata.append(planet_position[1])
    line.set_data(xdata, ydata)
    return (line,)


# Przygotowanie wykresu
fig, ax = plt.subplots()
ax.set_xlim(-10, 10)
ax.set_ylim(-10, 10)
ax.set_xlabel("X coordinate")
ax.set_ylabel("Y coordinate")
ax.set_title("Three-Star Planetary System")
ax.grid(True)

# Rysowanie gwiazd
for pos in star_positions:
    ax.plot(pos[0], pos[1], "yo", markersize=15)

# Linie trajektorii planety
xdata, ydata = [], []
(line,) = ax.plot([], [], "b-", label="Planet Path")

# Parametry animacji
dt = 0.001  # Krok czasowy
num_steps = 100000  # Liczba kroków symulacji

# Tworzenie animacji
ani = FuncAnimation(
    fig,
    animate,
    frames=num_steps,
    init_func=init,
    blit=True,
    interval=0.001,
    repeat=False,
)

plt.legend()
plt.show()
