function [t,y]=duffing
 
clear all;
close all;


opts = odeset('RelTol',1e-5,'AbsTol',1e-5);

% *-------*
delta=0.1;
betha=0.7;
F=8;
omg=1;
omg02=-1;
% *-------*
tspan = linspace(0,500,4000); 
y0 = [0; -0.001]; 

[t,y] = ode45(@funct1,tspan,y0,opts);
[t,u] = ode45(@funct1,tspan,[0; -0.002],opts);


figure
hold on;

plot(y(:,1),y(:,2),LineWidth=1.5);
plot(u(:,1),u(:,2),LineWidth=1.5);

title(['Rownanie Duffinga']);
xlabel('y');
ylabel("y'");
grid;
legend('przypadek #1','przypadek #2')


V=omg02*y(:,1).^2*0.5+betha*y(:,1).^4*0.25;
plot(y(:,1),V);
xlabel('y');
ylabel("V");
grid;
title('Wykres energii potencjalnej układu')
hold off;



% ------------------------
function dydt = funct1(t,y)
 dydt = [ y(2)
 -omg02*y(1)-betha*y(1)^3-delta*y(2)+F*cos(omg*t)];
 end
 end
