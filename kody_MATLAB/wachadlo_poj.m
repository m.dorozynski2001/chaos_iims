clear all;

g = 9.81; % Acceleration due to gravity (m/s^2)
L = 1.0; % Length of the pendulum (m)
theta0 = 0.5*pi; % Initial angle (radians)
tspan = linspace(0,4000,25000);
initial_conditions0 = [theta0 0];
initial_conditions1 = [theta0+0.002 0]; 

pendulum_eqns = @(t, y) [y(2); -g*sin(y(1))/L];

opcje = odeset('RelTol', 1e-6, 'AbsTol', 1e-6);
[t0, y] = ode45(pendulum_eqns, tspan, initial_conditions0,opcje);
[t1, v] = ode45(pendulum_eqns, tspan, initial_conditions1,opcje);


theta0 = y(:, 1);
theta1 = v(:, 1);


theta_deg0 = rad2deg(theta0);
theta_deg1 = rad2deg(theta1);


x0 = L * sin(theta0);
y0 = -L * cos(theta0);

x1 = L * sin(theta1);
y1 = -L * cos(theta1);


hold on;
comet(x1-x0, y1-y0);
xlabel('X [m]');
ylabel('Y [m]');
title('Różnica trajektorii');
grid on;


