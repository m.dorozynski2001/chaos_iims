img = imread('example.jpg'); 
img = rgb2gray(img);

[n, m] = size(img);

if n ~= m
    error('Obraz musi być kwadratowy');
end

num_iterations = 180;

figure;
imshow(uint8(img));

A = [2 1; 1 1];

for iter = 1:num_iterations
    new_img = zeros(n, m); 

    for x = 0:n-1
        for y = 0:m-1
            new_coords = mod(A * [x; y], n);
            new_x = new_coords(1) + 1;
            new_y = new_coords(2) + 1;
            new_img(new_x, new_y) = img(x+1, y+1);
        end
    end
    
    img = new_img;
    
    imshow(uint8(img));
    title(['Iteracja ' num2str(iter)]);
    pause(0.1); 
end
