clear all;
tspan=linspace(0,60,4000);
opcje = odeset('RelTol', 1e-6, 'AbsTol', 1e-6);
warunki=[0,1,20];
A=zeros(1,3);
s=10;
b=8/3;
r=28;


lorentz_system = @(t, dydt) [
s*dydt(2)-s*dydt(1);
-dydt(1)*dydt(3)+r*dydt(1)-dydt(2);
dydt(1)*dydt(2)-b*dydt(3)]

[t, A] = ode45(lorentz_system, tspan, warunki,opcje);
[t, B] = ode45(lorentz_system, tspan, [0,1.1,20],opcje);
hold on;
plot3(A(:, 1), A(:, 2), A(:, 3),'LineWidth',1.5);
xlabel('x');
ylabel('y');
zlabel('z');
grid on;
