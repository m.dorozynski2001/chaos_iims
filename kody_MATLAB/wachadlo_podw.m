clear all;

g = 9.81; 
L1 = 1.0;
L2 = 1.0; 
m1 = 1.0; 
m2 = 1.0; 
theta1_0 = 0.1; 
theta2_0 = 0;
tspan = linspace(0,60,4000);

warunkiP1 = [theta1_0, theta2_0, 0, 0]; 
warunkiP2 = [theta1_0+0.002, theta2_0, 0, 0]; 

rownania = @(t, y) uklad(t, y, g, L1, L2, m1, m2);
position_section = 0;

% Define options for the ODE solver
opcje = odeset('RelTol', 1e-6, 'AbsTol', 1e-6);

[t1, y] = ode45(rownania, tspan, warunkiP1,opcje);
[t2, v] = ode45(rownania, tspan, warunkiP2,opcje);

theta1 = y(:, 1);
theta2 = y(:, 2);
theta3 = v(:, 1);
theta4 = v(:, 2);

% plot(t1, theta1, 'LineWidth', 1.5);
% hold on;
% plot(t2, theta3, 'LineWidth', 1.5);
% xlim([540 600]);
% xlabel('Time (s)');
% ylabel('Amplitude (radians)');
% legend('w1','w2');
% title('Amplitude of Double Pendulum Over Time (First Pendulum)');
% grid on;

x1 = L1 * sin(theta1);
y1 = -L1 * cos(theta1);
x2 = x1 + L2 * sin(theta2);
y2 = y1 - L2 * cos(theta2);

x3 = L1 * sin(theta3);
y3 = -L1 * cos(theta3);
x4 = x3 + L2 * sin(theta4);
y4 = y3 - L2 * cos(theta4);
hold on;
plot(x4-x2, y4-y2, 'LineWidth', 1.5);
xlabel('X [m]');
ylabel('Y [m]');
title('Różnica trajektorii');
grid on;

%różnica między trajektoriami do plotowania
%opowiezieć o kilkus yttemach chaotycznych np system lorentza symulacja
%pokazanie go
%oscylator Duffinga
%bifurcation map - analiza kodu, podanie history diagramu

function dydt = uklad(t, y, g, L1, L2, m1, m2)
    theta1 = y(1);
    theta2 = y(2);
    omega1 = y(3);
    omega2 = y(4);
    
    dydt = zeros(4, 1);
    dydt(1) = omega1;
    dydt(2) = omega2;
    dydt(3) = (-g * (2 * m1 + m2) * sin(theta1) - m2 * g * sin(theta1 - 2 * theta2) - 2 * sin(theta1 - theta2) * m2 * (omega2^2 * L2 + omega1^2 * L1 * cos(theta1 - theta2))) / (L1 * (2 * m1 + m2 - m2 * cos(2 * theta1 - 2 * theta2)));
    dydt(4) = (2 * sin(theta1 - theta2) * (omega1^2 * L1 * (m1 + m2) + g * (m1 + m2) * cos(theta1) + omega2^2 * L2 * m2 * cos(theta1 - theta2))) / (L2 * (2 * m1 + m2 - m2 * cos(2 * theta1 - 2 * theta2)));
end
