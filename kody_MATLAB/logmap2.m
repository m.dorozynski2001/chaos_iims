
max_pokolen = 30; 
populacja = zeros(1, max_pokolen + 1);
vec_r = 1:0.5:3.6;

figure;
hold on;

popul_zero = rand(1);

for r_idx = 1:length(vec_r)
    r = vec_r(r_idx);
    popul_zero = rand(1);
    populacja(1) = popul_zero;
    for p = 2:max_pokolen + 1
        populacja(p) = r * populacja(p-1) * (1 - populacja(p-1));
    end
    plot(0:max_pokolen, populacja, 'DisplayName', sprintf('r = %.1f', r),'LineWidth',1.5);
end

hold off;
xlabel('Pokolenie (p)');
ylabel('Populacja (n)');
legend show;
grid on;
