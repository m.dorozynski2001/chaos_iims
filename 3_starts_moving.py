import numpy as np

# Ustawienia układu
star_positions = np.array(
    [
        [1, 2.1547005383792515],
        [-3.1547005383792515, -1.5773502691896258],
        [2.1547005383792515, -2.5773502691896258],
    ]
)  # Początkowe pozycje gwiazd
star_velocities = np.array(
    [[-1.0001, -1.0], [-1.0, -1.0001], [-2.0, -2.0]]
)  # Początkowe prędkości gwiazd
planet_position = np.array([0.0, 0.0])  # Początkowa pozycja planety
planet_velocity = np.array([-2.8, -3.0])  # Początkowa prędkość planety

# Masy gwiazd i planety
m_star1 = 10.0  # Masa pierwszej gwiazdy
m_star2 = 10.0  # Masa drugiej gwiazdy
m_star3 = 10.0  # Masa trzeciej gwiazdy
m_planet = 0.001  # Masa planety

G = 1.00001  # Stała grawitacyjna (wartość arbitralna dla uproszczenia)


# Funkcja do obliczania siły grawitacyjnej
def gravitational_force(pos1, pos2, m1, m2, G):
    r = np.linalg.norm(pos2 - pos1)
    if r == 0:
        return np.array([0.0, 0.0])
    force_magnitude = G * m1 * m2 / r**2
    force_direction = (pos2 - pos1) / r
    return force_magnitude * force_direction


# Funkcja do aktualizacji pozycji i prędkości planety i gwiazd
def update_positions_velocities(
    planet_pos, planet_vel, star_positions, star_velocities, masses, G, dt
):
    # Aktualizacja planety
    total_force_on_planet = np.array([0.0, 0.0])
    for i, star_pos in enumerate(star_positions):
        force = gravitational_force(planet_pos, star_pos, m_planet, masses[i], G)
        total_force_on_planet += force

    planet_acceleration = total_force_on_planet / m_planet
    planet_vel += planet_acceleration * dt
    planet_pos += planet_vel * dt

    # Aktualizacja gwiazd
    new_star_positions = star_positions.copy()
    new_star_velocities = star_velocities.copy()

    for i in range(len(star_positions)):
        total_force_on_star = np.array([0.0, 0.0])
        for j in range(len(star_positions)):
            if i != j:
                force = gravitational_force(
                    star_positions[i], star_positions[j], masses[i], masses[j], G
                )
                total_force_on_star += force

        star_acceleration = total_force_on_star / masses[i]
        new_star_velocities[i] += star_acceleration * dt
        new_star_positions[i] += new_star_velocities[i] * dt

    return planet_pos, planet_vel, new_star_positions, new_star_velocities


# Funkcja inicjalizacyjna do animacji
def init():
    line_planet.set_data([], [])
    for star_line in star_lines:
        star_line.set_data([], [])
    return [line_planet] + star_lines


# Funkcja aktualizująca do animacji
def animate(i):
    global planet_position, planet_velocity, star_positions, star_velocities
    planet_position, planet_velocity, star_positions, star_velocities = (
        update_positions_velocities(
            planet_position,
            planet_velocity,
            star_positions,
            star_velocities,
            [m_star1, m_star2, m_star3],
            G,
            dt,
        )
    )

    xdata_planet.append(planet_position[0])
    ydata_planet.append(planet_position[1])
    line_planet.set_data(xdata_planet, ydata_planet)

    for j, star_line in enumerate(star_lines):
        xdata_stars[j].append(star_positions[j][0])
        ydata_stars[j].append(star_positions[j][1])
        star_line.set_data(xdata_stars[j], ydata_stars[j])

    return [line_planet] + star_lines


# Przygotowanie wykresu
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation

fig, ax = plt.subplots()
ax.set_xlim(-100, 10)
ax.set_ylim(-100, 10)
ax.set_xlabel("X coordinate")
ax.set_ylabel("Y coordinate")
ax.set_title("Three-Star Planetary System with Moving Stars")
ax.grid(True)

# Linie trajektorii planety
xdata_planet, ydata_planet = [], []
(line_planet,) = ax.plot([], [], "b-", label="Planet Path")

# Linie trajektorii gwiazd
xdata_stars = [[] for _ in range(len(star_positions))]
ydata_stars = [[] for _ in range(len(star_positions))]
star_lines = [ax.plot([], [], "r-")[0] for _ in range(len(star_positions))]

# Początkowe pozycje gwiazd
for pos in star_positions:
    ax.plot(pos[0], pos[1], "yo", markersize=15)

# Parametry animacji
dt = 0.01  # Krok czasowy
num_steps = 1000000  # Liczba kroków symulacji

# Tworzenie animacji
ani = FuncAnimation(
    fig,
    animate,
    frames=num_steps,
    init_func=init,
    blit=True,
    interval=0.01,
    repeat=False,
)

plt.legend()
plt.show()
