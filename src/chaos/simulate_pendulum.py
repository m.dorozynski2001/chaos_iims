import matplotlib.pyplot as plt
import numpy as np
from matplotlib import animation


def visualize_pendulum(theta):
    # Inicjalizacja animacji
    fig = plt.figure()
    ax = fig.add_subplot(
        111, aspect="equal", autoscale_on=False, xlim=(-1.5, 1.5), ylim=(-1.5, 1.5)
    )
    ax.grid()

    (line,) = ax.plot([], [], "o-", lw=2)
    line.set_color("blue")

    # Utworzenie animacji
    _ = animation.FuncAnimation(
        fig,
        update_line,
        frames=len(theta),
        fargs=(theta, line),
        interval=dt * 1000,
        blit=True,
    )

    plt.show()


def update_line(num, data, line):
    line.set_data([0, np.sin(data[num])], [0, -np.cos(data[num])])
    return (line,)


def simulate_pendulum(theta0, omega0, L, g, dt, time_steps):
    """
    Symuluje ruch wahadła podwójnego
    theta0: początkowy kąt wychylenia [rad]
    omega0: początkowa prędkość kątowa [rad/s]
    L: długość wahadła [m]
    g: przyspieszenie ziemskie [m/s^2]
    dt: krok czasowy [s]
    time_steps: liczba kroków czasowych
    """
    theta = np.zeros(time_steps)
    omega = np.zeros(time_steps)
    theta[0] = theta0
    omega[0] = omega0

    for i in range(1, time_steps):
        alpha = -(g / L) * np.sin(theta[i - 1])
        omega[i] = omega[i - 1] + alpha * dt
        theta[i] = theta[i - 1] + omega[i] * dt

    return theta, omega


# Parametry symulacji
theta0 = 0.1  # Początkowy kąt wychylenia [rad]
omega0 = 0.0  # Początkowa prędkość kątowa [rad/s]
L = 1.0  # Długość wahadła [m]
g = 9.81  # Przyspieszenie ziemskie [m/s^2]
dt = 0.01  # Krok czasowy [s]
time_steps = 1000  # Liczba kroków czasowych

# Symulacja wahadła chaotycznego
theta_chaotic, _ = simulate_pendulum(theta0, omega0 + 6.3, L, g, dt, time_steps)

# Symulacja wahadła niechaotycznego
theta_nonchaotic, _ = simulate_pendulum(theta0, omega0, L, g, dt, time_steps)

# Wykresy
time = np.arange(0, time_steps * dt, dt)
plt.figure(figsize=(10, 5))
plt.plot(time, theta_chaotic, label="Wahadło chaotyczne")
plt.plot(time, theta_nonchaotic, label="Wahadło niechaotyczne")
plt.title("Symulacja ruchu wahadła")
plt.xlabel("Czas [s]")
plt.ylabel("Kąt wychylenia [rad]")
plt.legend()
plt.grid(True)
plt.show()

visualize_pendulum(theta_nonchaotic)
visualize_pendulum(theta_chaotic)
